<?php

/**
 * @file
 * Hooks for lw_groups.
 */

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;

/**
 * A hook to change the access result check.
 *
 * See lw_groups/src/AccessCheck.php line 32 ish.
 *
 * @param Drupal\Core\Access\AccessResult $result
 *   Access result.
 * @param Drupal\Core\Session\AccountInterface $account
 *   Account.
 */
function hook_lw_groups_approve_access(AccessResult &$result, AccountInterface $account) {
  // Here is the current $result whic could be AccessResult::forbidden(); ect.
}

/**
 * See Drupal\lw_groups\Plugin\Block\ApproveUserBlock line 144 ish.
 *
 * @param \Drupal\user\UserInterface $current_user
 *   The current logged in user.
 * @param \Drupal\user\UserInterface $user_to_approve
 *   The user who is to be apposed.
 * @param bool $has_access
 *   True or false.
 */
function hook_lw_groups_approve_access_block_link(UserInterface $current_user, UserInterface $user_to_approve, &$has_access) {
  // Here change $has_access to TRUE or FALSE based on what ever.
  /*
  if ($has_access == TRUE) {
  // Change my_field to your field name.
  if (!in_array('administrator', $current_user->getRoles())) {
  $current_user_term_id = $current_user->my_field->target_id;
  $user_to_approve_term_id = $user_to_approve->my_field->target_id;
  // If this dude is not in same group dont show link.
  if ($current_user_term_id != $user_to_approve_term_id) {
  $has_access = FALSE;
  }
  }
  }
   */
}

/**
 * Act on user before it is saved in ajax modal.
 *
 * See "lw_groups_membership_approve_form_ajax_submit"
 *
 * @param array $form
 *   The form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   Formstate.
 * @param \Drupal\user\UserInterface $user
 *   The fully loaded user.
 */
function hook_lw_groups_ajax_modal_approve_form_save(array &$form, FormStateInterface $form_state, UserInterface &$user) {
  // Act on the $user before $user->save() is called.
}
