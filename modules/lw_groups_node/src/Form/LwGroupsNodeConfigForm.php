<?php

namespace Drupal\lw_groups_node\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LwGroupsNodeConfigForm.
 */
class LwGroupsNodeConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityManager = $container->get('entity.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'lw_groups_node.lwgroupsnodeconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lw_groups_node_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $error = FALSE;
    $config = $this->config('lw_groups_node.lwgroupsnodeconfig');

    $default_data = $config->get('form_data');
    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    // If you need to display them in a drop down:
    $vocab = lw_groups_get_config()->get('vocab');

    $form['valid_c_types'] = [
      '#type' => 'fieldset',
      '#title' => 'Select Content types fields',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $content_options = [];
    $node_fields = [];
    foreach ($node_types as $node_type) {
      $valid = FALSE;

      $all_node_fields = $this->entityManager->getFieldDefinitions('node', $node_type->id());
      // Here check all fields to make sure we have one enity ref field.
      foreach ($all_node_fields as $field_name => $field_config) {
        if (strpos($field_name, 'field_') !== FALSE) {
          // Check field type.
          if ($field_config->get('field_type') === 'entity_reference') {

            $handler_settings = $field_config->getSetting('handler_settings');
            if (in_array($vocab, $handler_settings['target_bundles'])) {
              $node_fields[$node_type->id()][$field_name] = $field_name;
              $valid = TRUE;
            }
          }
        }
      }

      if ($valid === TRUE) {
        $content_options[$node_type->id()] = $node_type->label();
      }
    }

    if (count($node_fields) == 0) {
      \Drupal::messenger()->addError($this->t('You do not have an "entity_reference" field on any node types. <br/> Please add a taxonomy term reference field on a content type you want to use for group @vocab', [
        '@vocab' => $vocab,
      ]
      ));
      $error = TRUE;
    }
    else {

      foreach ($node_fields as $node_type => $options) {
        $key = $node_type . '--valid_field';
        $form['valid_c_types'][$key] = [
          '#default_value' => $default_data[$key] ?? NULL,
          '#required' => TRUE,
          '#type' => 'select',
          '#options' => $options,
          '#title' => $content_options[$node_type] . ' Select Entity Reference field',
          '#description' => $this->t('Please ensure this field is the right taxonomy term ref field as your group <br/> <b>Note:</b> this field will be auto-populated by the authors value see "lw_groups_node_node_create"'),
        ];
      }

    }

    // Here filter out content types that dont have a valid taxonomy ref field.
    if (empty($vocab)) {
      drupal_set_message('Please select  vocabulary ', 'error');
      $error = TRUE;
    }
    else {
      $terms = lw_groups_node_get_terms($vocab);
      $role_options = [];
      $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
      foreach ($roles as $r_key => $role) {
        $role_options[$r_key] = $role->label();
      }
      unset($role_options['anonymous']);

      $actions = [
        'create' => 'Create',
        'update' => 'Update',
        'delete' => 'Delete',
      ];

      $form['access_type'] = [
        '#type' => 'radios',
        '#default_value' => $default_data['access_type'] ?? NULL,
        '#options' => [
          'all_same' => 'All Groups share permissions',
          'na' => '@TODO allow individual select ... see code',
          // @TODO this later see line 200 ish .
          // 'individual' => 'set individual',
        ],
        '#required' => TRUE,
      ];

      $form['content_items'] = [
        '#type' => 'fieldset',
        '#title' => 'All Groups share same Permissions for content types',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#states' => [
          // Hide the settings when the cancel notify checkbox is disabled.
          'visible' => [
            ':input[name="access_type"]' => ['value' => 'all_same'],
          ],
        ],
      ];

      $form['content_items']['content_types'] = [
        '#type' => 'checkboxes',
        '#options' => $content_options,
        '#default_value' => $default_data['content_types'] ?? [],
      ];

      foreach ($content_options as $node_type => $node_title) {
        foreach ($role_options as $r_key => $r_title) {
          $key = $node_type . '~~' . $r_key;
          $form['content_items'][$key] = [
            '#default_value' => $default_data[$key] ?? [],
            '#title' => $node_title . ' - ' . $r_title,
            '#type' => 'checkboxes',
            '#options' => $actions,
            '#states' => [
              // Hide the settings when the cancel notify checkbox is disabled.
              'visible' => [
                ':input[name="content_types[' . $node_type . ']"]' => ['checked' => TRUE],
              ],
            ],
          ];
        }
      }

      // @TODO this later.
      /*
      foreach ($terms as $term_id => $term) {
      $form[$term_id] = array(
      '#type' => 'fieldset',
      '#title' => $term->label(),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#states' => [
      // Hide the settings when the cancel notify checkbox is disabled.
      'visible' => [
      ':input[name="access_type"]' => ['value' => 'individual'],
      ],
      ],
      );


      $ukey = 'term~'. $term_id;
      $form[$term_id][$ukey] = [
      '#type' => 'checkboxes',
      '#title' => ''.$term->label(),
      '#options' => $content_options,
      ];

      foreach ($content_options as $node_type => $node_title) {
      foreach ($role_options as $r_key => $r_title) {
      $key = $term_id.'--'. $node_type.'~~'.$r_key;
      $form[$term_id][$key] = [
      '#title' => $node_title. ' - '.$r_title,
      '#type' => 'checkboxes',
      '#options' => $actions,
      '#states' => [
      // Hide the settings when the cancel notify checkbox is disabled.
      'visible' => [
      ':input[name="'.$ukey.'['.$node_type.']"]' => ['checked' => TRUE],
      ],
      ],
      ];
      }
      }
      }
       */
    }

    if ($error == FALSE) {
      return parent::buildForm($form, $form_state);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $user_input = $form_state->getUserInput();

    unset($user_input['form_build_id']);
    unset($user_input['form_token']);
    unset($user_input['form_id']);
    unset($user_input['op']);
    $content_type_fields = [];
    $content_access = [];
    foreach ($user_input as $key => $value) {
      // Save content_type_fields.
      if (strpos($key, '--valid_field') !== FALSE) {
        $explode = explode('--valid_field', $key);
        $c = $explode[0];
        $content_type_fields[$c] = $value;
      }
      foreach ($user_input['content_types'] as $type) {
        $string = $type . '~~';
        if (strpos($key, $string) !== FALSE) {
          $explode = explode($string, $key);
          $role_id = $explode[1];
          // Check value.
          $ops = [];
          foreach ($value as $opp_key => $opp_val) {
            if (!empty($opp_val)) {
              $ops[] = $opp_val;
            }
          }
          if (!empty($ops)) {
            $content_access[$type][$role_id] = $ops;
          }
        }
      }
    }
    $set_type = $user_input['access_type'];
    // @ todo hande individual or not.
    if ($set_type != 'all_same') {
      drupal_set_message('No don\'t do that', 'error');
    }
    else {
      // Save.
      $this->config('lw_groups_node.lwgroupsnodeconfig')
        ->set('access', $content_access)
        ->set('access_type', $user_input['access_type'])
        ->set('content_types', $user_input['content_types'])
        ->set('content_type_fields', $content_type_fields)
        ->set('form_data', $user_input)
        ->save();
    }
  }

}
