<?php

namespace Drupal\lw_groups_node;

use Drupal\Core\Session\UserSession;

/**
 * Helper class to do operations on the user.
 */
class UserAccountHelpers extends UserSession {

  /**
   * Normal user session.
   *
   * @var \Drupal\Core\Session\UserSession
   */
  public $userSession;

  /**
   * The loaded user.
   *
   * @var user
   */
  public $user;


  /**
   * The config defined by lw_groups.
   *
   * @var array
   */
  public $parentConfig;

  /**
   * The config defined by lw_groups_node.
   *
   * @var array
   */
  public $config;

  /**
   * UserAccountHelpers constructor.
   *
   * @param \Drupal\Core\Session\UserSession $userSession
   *   The normal user session.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(UserSession $userSession) {
    $this->userSession = $userSession;
    $this->user = \Drupal::entityTypeManager()->getStorage('user')->load($userSession->id());
    $this->config = lw_groups_node_get_config();
    $this->parentConfig = lw_groups_get_config();
  }

  /**
   * Helper function to check if the user is approved.
   *
   * @return bool
   *   Returns true or false.
   */
  public function isUserApproved() {
    $results = FALSE;
    if ($this->user->field_lw_groups_approval->value == 1) {
      $results = TRUE;
    }
    return $results;
  }

  /**
   * Helper function to get the user group field term id.
   *
   * @return string|null
   *   This returns a term id as as string or null.
   */
  public function getGroupTermId() {
    $field = $this->parentConfig->get('user_field');
    return $this->user->{$field}->target_id ?? NULL;
  }

  /**
   * Helper function to get the user group field term.
   *
   * @return term|null
   *   This returns the term or null.
   */
  public function getGroupTerm() {
    $field = $this->parentConfig->get('user_field');
    $term = NULL;
    if (!empty($this->user->{$field}->target_id)) {
      $term = $this->user->{$field}->referencedEntities()[0];
    }
    return $term;
  }

}
