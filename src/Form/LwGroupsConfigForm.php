<?php

namespace Drupal\lw_groups\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LwGroupsConfigForm.
 */
class LwGroupsConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Provides messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->currentUser = $container->get('current_user');
    $instance->entityManager = $container->get('entity.manager');
    $instance->messenger = $container->get('messenger');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'lw_groups.lwgroupsconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lw_groups_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $error = FALSE;
    $config = $this->config('lw_groups.lwgroupsconfig');
    $vocabularies = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();
    $vocab_options = [];
    foreach ($vocabularies as $key => $vocabulary) {
      $vocab_options[$key] = $vocabulary->label();
    }

    $form['vocab'] = [
      '#type' => 'select',
      '#title' => $this->t('Please Select which vocabulary to use'),
      '#description' => $this->t('Select which vocabulary to use for lightweight groups.'),
      '#options' => $vocab_options,
      '#size' => 1,
      '#required' => TRUE,
      '#default_value' => $config->get('vocab'),
    ];

    $user_fields = $this->entityManager->getFieldDefinitions('user', 'user');
    // Get list of user made fields.
    $created_fields = [];
    $has_field_lw_groups_approval = FALSE;
    foreach ($user_fields as $field_key => $field_config) {
      if (strpos($field_key, 'field_') !== FALSE) {
        $created_fields[$field_key] = $field_config;
        if ($field_key == 'field_lw_groups_approval') {
          $has_field_lw_groups_approval = TRUE;
        }
      }
    }

    $field_select_options = [];
    if (empty($created_fields)) {
      $this->messenger->addError($this->t('You have no custom fields added to the user entity. <br/> Please add a taxonomy term reference field on the user entity <a href="/admin/config/people/accounts/fields">here.</a>'));
      $error = TRUE;
    }
    else {
      // Here make sure there taxonomy ref field.
      $has_entity_reference_field = FALSE;
      foreach ($created_fields as $field_key => $field_config) {
        if (is_object($field_config)) {
          if ($field_config->get('field_type') === 'entity_reference') {
            $has_entity_reference_field = TRUE;
            // @todo here could check the settings etc.
            $field_select_options[$field_key] = $field_key;
          }
        }
      }

      if ($has_entity_reference_field == FALSE) {
        $this->messenger->addError($this->t('You do not have an "entity_reference" field on the  user entity. <br/> Please add a taxonomy term reference field on the user entity <a href="/admin/config/people/accounts/fields">here.</a>'));
        $error = TRUE;
      }
    }

    $form['user_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Please Select the entity reference user field.'),
      '#description' => $this->t('You need select the entity reference field that you created on the user entity.'),
      '#options' => $field_select_options,
      '#required' => TRUE,
      '#size' => 1,
      '#default_value' => $config->get('user_field'),
    ];

    $role_options = [];
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    foreach ($roles as $r_key => $role) {
      $role_options[$r_key] = $role->label();
    }

    $form['role_allow_field_access'] = [
      '#title' => $this->t('Which Role is allowed to Approve new members'),
      '#type' => 'checkboxes',
      '#options' => $role_options,
      '#default_value' => $config->get('role_allow_field_access') ?? [],
      '#description' => $this->t('As this module comes with field "field_lw_groups_approval" which is a boolean to see if the group member is approved or not.<br/> Please select which role can set value for this field.<br/> this will grant the permission lw groups - approve group membership'),
    ];

    $form['info'] = [
      '#markup' => $this->t('<br/>This will provide a url "/user/(user_id)/group-approve" to approve the user to the group. which is a new form mode.<br/>'),
    ];

    $form['allow_term_edit'] = [
      '#title' => $this->t('Allow Special role to edit the group taxonomy Term'),
      '#description' => $this->t('This will allow <b>a group member</b> with special role to edit the taxonomy term. only the update operation see function "lw_groups_taxonomy_term_access"'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('allow_term_edit'),
    ];

    unset($role_options['anonymous']);
    $form['term_edit_role'] = [
      '#title' => $this->t('Which Role is allowed to Edit the Taxonomy term'),
      '#type' => 'checkboxes',
      '#options' => $role_options,
      '#default_value' => $config->get('term_edit_role') ?? [],
      // '#description' => $this->t(''),
      '#states' => [
        // Hide the settings when the cancel notify checkbox is disabled.
        'visible' => [
          ':input[name="allow_term_edit"]' => ['checked' => TRUE],
        ],
      ],
    ];

    if ($error == FALSE && $has_field_lw_groups_approval == TRUE) {
      $form = parent::buildForm($form, $form_state);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $access_roles = $form_state->getValue('role_allow_field_access');
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    foreach ($access_roles as $r_key => $r_value) {
      if (!empty($r_value)) {
        // Add.
        if (!$roles[$r_key]->hasPermission('lw groups - approve group membership')) {
          $roles[$r_key]->grantPermission('lw groups - approve group membership');
          $roles[$r_key]->save();
        }
      }
      else {
        // Remove.
        if ($roles[$r_key]->hasPermission('lw groups - approve group membership')) {
          $roles[$r_key]->revokePermission('lw groups - approve group membership');
          $roles[$r_key]->save();
        }
      }
    }

    $this->config('lw_groups.lwgroupsconfig')
      ->set('vocab', $form_state->getValue('vocab'))
      ->set('user_field', $form_state->getValue('user_field'))
      ->set('role_allow_field_access', $access_roles)
      ->set('allow_term_edit', $form_state->getValue('allow_term_edit'))
      ->set('term_edit_role', $form_state->getValue('term_edit_role'))
      ->save();
    $this->messenger->addMessage($this->t('Settings Saved'));
    // Need to flush cache here.
    drupal_flush_all_caches();
  }

}
