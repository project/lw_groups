<?php

namespace Drupal\lw_groups;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Builds an access check call back.
 */
class AccessCheck {

  /**
   * Checks access for a Approve Group Members page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function accessApproveGroupMembers(AccountInterface $account) {
    $result = AccessResult::allowed();
    if (empty($account->id())) {
      $result = AccessResult::forbidden();
    }
    if (!$account->hasPermission('lw groups - approve group membership')) {
      $result = AccessResult::forbidden();
    }
    \Drupal::moduleHandler()->invokeAll('lw_groups_approve_access', [
      &$result,
      $account,
    ]);
    return $result;
  }

}
