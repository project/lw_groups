<?php

namespace Drupal\lw_groups\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'ApproveUserBlock' block.
 *
 * @Block(
 *  id = "lw_groups_approve_user_block",
 *  admin_label = @Translation("Group Approve User Block"),
 * )
 */
class ApproveUserBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->requestStack = $container->get('request_stack');
    $instance->currentUser = $container->get('current_user');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'link_style' => 'ajax',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['link_style'] = [
      '#type' => 'radios',
      '#title' => $this->t('Link Style'),
      '#description' => $this->t('What style to use for link'),
      '#options' => [
        'ajax' => $this->t('ajax'),
        'direct' => $this->t('Direct'),
        'new_window' => $this->t('New Window'),
      ],
      '#default_value' => $this->configuration['link_style'],
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['link_style'] = $form_state->getValue('link_style');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $link_style = $this->configuration['link_style'];
    $build = [];
    $request = $this->requestStack->getCurrentRequest();
    $module_config = lw_groups_get_config();
    $field = $module_config->get('user_field');
    $user = $request->get('user');
    $link = '';
    // Make default.
    if (!empty($user) && !empty($field)) {
      if (!empty($user->{$field}->target_id)) {
        // This is a user signed up tp a group.
        $text = 'Approve User';
        if (!empty($user->field_lw_groups_approval->value)) {
          $text = 'Un-Approve User';
        }
        switch ($link_style) {
          case 'ajax':
            $link = '<a id="lw-approve-member-link" href="/user/' . $user->id() . '/group-approve" class="button use-ajax" data-dialog-type="modal" data-dialog-options="{&quot;width&quot;:700}">' . $text . '</a>';
            break;

          case 'direct':
            $link = '<a id="lw-approve-member-link" href="/user/' . $user->id() . '/group-approve">' . $text . '</a>';
            break;

          case 'new_window':
            $link = '<a id="lw-approve-member-link" href="/user/' . $user->id() . '/group-approve" target="_blank">' . $text . '</a>';
            break;
        }

        $role_allow_field_access = $module_config->get('role_allow_field_access');
        $has_access = FALSE;
        $current_user_id = $this->currentUser->id();
        $current_user = $this->entityTypeManager->getStorage('user')->load($current_user_id);

        if (!empty($role_allow_field_access)) {
          foreach ($current_user->getRoles() as $role_id) {
            if (in_array($role_id, $role_allow_field_access)) {
              $has_access = TRUE;
            }
          }
        }
        if ($current_user_id == 1) {
          $has_access = TRUE;
        }

        // Here offer hook for developers to change requirements.
        \Drupal::moduleHandler()->invokeAll('lw_groups_approve_access_block_link', [
          $current_user,
          $user,
          &$has_access,
        ]);

        if ($has_access == TRUE) {
          $build['link'] = [
            '#markup' => $link,
          ];
        }
        else {
          $build['link'] = [
            '#markup' => ' ',
          ];
        }
      }
    }

    return $build;
  }

}
