<?php

/**
 * @file
 * Contains lw_groups.module.
 */

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_help().
 */
function lw_groups_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the lw_groups module.
    case 'help.page.lw_groups':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('lightweight groups') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_entity_field_access().
 */
function lw_groups_entity_field_access($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
  $result = AccessResult::neutral();
  if ($field_definition->getName() == 'field_lw_groups_approval') {
    $config = lw_groups_get_config();
    $allowed_roles = $config->get('role_allow_field_access');
    $has_role = FALSE;
    if ($account->id() && $account->id() == 1) {
      $has_role = TRUE;
    }
    $account_roles = $account->getRoles();
    foreach ($allowed_roles as $role) {
      if (!empty($role)) {
        if (in_array($role, $account_roles)) {
          $has_role = TRUE;
        }
      }
    }
    if ($operation == 'edit' && $has_role != TRUE) {
      $result = AccessResult::forbidden();
    }
  }
  return $result->addCacheContexts(['user.roles:administrator']);
}

/**
 * Helper function to get the module config.
 *
 * @return mixed
 *   This returns the config form the admin form.
 */
function lw_groups_get_config() {
  return \Drupal::service('config.factory')->get('lw_groups.lwgroupsconfig');
}

/**
 * Helper function to get user group Id.
 *
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The account.
 *
 * @return bool
 *   Boolean.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function lw_groups_get_user_group_id(AccountInterface $account) {
  $results = FALSE;
  $field = lw_groups_get_config()->get('user_field');
  $user = \Drupal::entityTypeManager()->getStorage('user')->load($account->id());
  if (!empty($field)) {
    if (!empty($user->{$field}->target_id)) {
      $results = $user->{$field}->target_id;
    }
  }
  return $results;
}

/**
 * Helper to check if the user is approved or not.
 *
 * @param \Drupal\Core\Session\AccountInterface $account
 *   Account.
 *
 * @return bool
 *   Boolean.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function lw_groups_is_user_approved(AccountInterface $account) {
  $results = FALSE;
  $user = \Drupal::entityTypeManager()->getStorage('user')->load($account->id());
  if ($user->field_lw_groups_approval->value == 1) {
    $results = TRUE;
  }
  return $results;
}

/**
 * Implements hook_form_alter().
 */
function lw_groups_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id === 'user_approve_group_membership_form') {
    $user = $form_state->getFormObject()->getEntity();
    $name = $user->getDisplayName();
    $group_name = '';
    if (!empty(\Drupal::request()->get('_wrapper_format')) && !empty($form['field_lw_groups_approval']['widget'])) {
      // @TODO make this work better.
      $form['actions']['submit']['#ajax'] = [
        'callback' => 'lw_groups_membership_approve_form_ajax_submit',
        'event' => 'click',
      ];
    }
    $config = lw_groups_get_config();
    $field = $config->get('user_field');
    $form['name'] = [
      '#markup' => t('<b>User:</b> @user', [
        '@user' => $name,
      ]),
      '#weight' => -99,
    ];
    if (!empty($field)) {
      if (!empty($user->{$field}->target_id)) {
        $group_term = $user->{$field}->referencedEntities();
        if (!empty($group_term[0])) {
          $group_name = $group_term[0]->get('name')->value;
          $form['group_name'] = [
            '#markup' => t('<br/><b>Group Name:</b> @gname', [
              '@gname' => $group_name,
            ]),
            '#weight' => -99,
          ];
        }
      }
      else {
        \Drupal::messenger()->addError(t('This user has not set a value for "@field"', [
          '@field' => $field,
        ]));
      }
    }
  }
}

/**
 * Implements hook_form_submit().
 */
function lw_groups_membership_approve_form_ajax_submit(&$form, FormStateInterface $form_state) {
  $user = $form_state->getFormObject()->getEntity();
  $approval = $form_state->getValue('field_lw_groups_approval');
  $text = 'Approve User';
  if (!empty($approval) && $approval['value'] == 1) {
    $text = 'Un-Approve User';
  }
  $user->set('field_lw_groups_approval', $approval['value']);
  \Drupal::moduleHandler()->invokeAll('lw_groups_ajax_modal_approve_form_save', [
    $form,
    $form_state,
    &$user,
  ]);
  $user->save();

  $response = new AjaxResponse();
  $response->addCommand(new HtmlCommand('#lw-approve-member-link', $text));
  $response->addCommand(new CloseDialogCommand());
  return $response;
}

/**
 * Implements hook_entity_type_alter().
 */
function lw_groups_entity_type_alter(array &$entity_types) {
  $form_modes = \Drupal::service('entity_display.repository')->getAllFormModes();
  foreach ($form_modes as $entity_type => $display_modes) {
    /* @var \Drupal\Core\Entity\EntityTypeInterface $entity */
    $entity = $entity_types[$entity_type];
    foreach ($display_modes as $machine_name => $form_display) {
      if ($machine_name != 'register') {
        // Get the correct canonical path to add operation.
        $path = $entity->getLinkTemplate('canonical') . "/$machine_name";
        $default_handler_class = $entity->getHandlerClasses()['form']['default'];
        $entity->setFormClass($machine_name, $default_handler_class)
          ->setLinkTemplate($machine_name, $path);
      }
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_access().
 */
function lw_groups_taxonomy_term_access(EntityInterface $entity, $operation, AccountInterface $account) {
  $result = AccessResult::neutral();
  if ($operation == 'update') {
    $allow = lw_groups_get_config()->get('allow_term_edit');
    if (!empty($allow) && $allow == 1) {
      $term_edit_role = lw_groups_get_config()->get('term_edit_role');
      $user_term_id = lw_groups_get_user_group_id($account);
      $user_approved = lw_groups_is_user_approved($account);
      // Make sure there a member of this group only.
      if (!empty($user_term_id) && $user_term_id == $entity->id() && $user_approved == TRUE) {
        if (!empty($term_edit_role)) {
          $roles = $account->getRoles();
          foreach ($term_edit_role as $role_key) {
            if (!empty($role_key)) {
              if (in_array($role_key, $roles)) {
                $result = AccessResult::allowed();
              }
            }
          }
        }
      }
    }
  }
  return $result;
}
